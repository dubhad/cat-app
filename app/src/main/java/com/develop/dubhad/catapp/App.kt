package com.develop.dubhad.catapp

import android.app.Application
import com.develop.dubhad.catapp.utilities.DelegatesExtensions

class App : Application() {

    companion object {
        var instance: App by DelegatesExtensions.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
