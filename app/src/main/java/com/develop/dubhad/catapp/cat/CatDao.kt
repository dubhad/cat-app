package com.develop.dubhad.catapp.cat

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface CatDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCat(cat: Cat)

    @Delete
    fun deleteCat(cat: Cat)

    @Query("SELECT * FROM cats")
    fun getAllCats(): Observable<List<Cat>>
}
