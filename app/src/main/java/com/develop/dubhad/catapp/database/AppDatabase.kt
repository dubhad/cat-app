package com.develop.dubhad.catapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.cat.CatDao

@Database(entities = [Cat::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun catDao(): CatDao
}
