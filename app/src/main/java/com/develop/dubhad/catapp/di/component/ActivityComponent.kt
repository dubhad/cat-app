package com.develop.dubhad.catapp.di.component

import com.develop.dubhad.catapp.di.module.ActivityModule
import com.develop.dubhad.catapp.ui.main.MainActivity
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)
}
