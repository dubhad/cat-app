package com.develop.dubhad.catapp.di.component

import com.develop.dubhad.catapp.di.module.FragmentModule
import com.develop.dubhad.catapp.ui.catlist.CatListFragment
import com.develop.dubhad.catapp.ui.favouritelist.FavouriteListFragment
import dagger.Component

@Component(modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(catListFragment: CatListFragment)

    fun inject(favouriteListFragment: FavouriteListFragment)
}
