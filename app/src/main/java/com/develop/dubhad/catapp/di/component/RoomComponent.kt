package com.develop.dubhad.catapp.di.component

import com.develop.dubhad.catapp.di.module.RoomModule
import com.develop.dubhad.catapp.ui.catlist.CatListPresenter
import com.develop.dubhad.catapp.ui.favouritelist.FavouriteListPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RoomModule::class])
interface RoomComponent {

    fun inject(catListPresenter: CatListPresenter)

    fun inject(favouriteListPresenter: FavouriteListPresenter)
}
