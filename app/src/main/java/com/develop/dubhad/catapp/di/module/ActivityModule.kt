package com.develop.dubhad.catapp.di.module

import androidx.appcompat.app.AppCompatActivity
import com.develop.dubhad.catapp.ui.main.MainContract
import com.develop.dubhad.catapp.ui.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: AppCompatActivity) {

    @Provides
    fun provideActivity(): AppCompatActivity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }
}
