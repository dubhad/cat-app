package com.develop.dubhad.catapp.di.module

import com.develop.dubhad.catapp.network.CatApi
import com.develop.dubhad.catapp.ui.catlist.CatListContract
import com.develop.dubhad.catapp.ui.catlist.CatListPresenter
import com.develop.dubhad.catapp.ui.favouritelist.FavouriteListContract
import com.develop.dubhad.catapp.ui.favouritelist.FavouriteListPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {

    @Provides
    fun provideCatApi(): CatApi {
        return CatApi.instance
    }

    @Provides
    fun provideCatListPresenter(): CatListContract.Presenter {
        return CatListPresenter()
    }

    @Provides
    fun provideFavouriteListPresenter(): FavouriteListContract.Presenter {
        return FavouriteListPresenter()
    }
}
