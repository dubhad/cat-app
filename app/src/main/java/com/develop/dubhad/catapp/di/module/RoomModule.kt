package com.develop.dubhad.catapp.di.module

import android.app.Application
import androidx.room.Room
import com.develop.dubhad.catapp.cat.CatDao
import com.develop.dubhad.catapp.database.AppDatabase
import com.develop.dubhad.catapp.utilities.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(app: Application) {

    private var database: AppDatabase =
        Room.databaseBuilder(app, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideRoomDatabase(): AppDatabase {
        return database
    }

    @Singleton
    @Provides
    fun provideCatDao(db: AppDatabase): CatDao {
        return db.catDao()
    }
}
