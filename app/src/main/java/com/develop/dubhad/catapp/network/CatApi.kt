package com.develop.dubhad.catapp.network

import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.utilities.BASE_URL
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface CatApi {

    @GET("images/search?limit=10")
    fun getCats(): Observable<List<Cat>>

    companion object {
        val instance: CatApi by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
            retrofit.create(CatApi::class.java)
        }
    }
}
