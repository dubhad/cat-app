package com.develop.dubhad.catapp.ui.catlist

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.develop.dubhad.catapp.R
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.ui.main.MainActivity.Companion.downloadUrl
import com.develop.dubhad.catapp.utilities.PermissionUtil
import com.develop.dubhad.catapp.utilities.REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE
import com.develop.dubhad.catapp.utilities.WRITE_EXTERNAL_STORAGE_PERMISSION

class CatListAdapter(private val presenter: CatListContract.Presenter) :
    ListAdapter<Cat, CatListAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_cat,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cat = getItem(position)
        holder.apply {
            bind(cat, presenter)
            itemView.tag = cat.id
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image: ImageView = itemView.findViewById(R.id.item_cat_image)
        private val addToFavButton: ImageButton = itemView.findViewById(R.id.item_add_to_fav_button)
        private val downloadImageButton: ImageButton =
            itemView.findViewById(R.id.item_download_button)

        fun bind(item: Cat, presenter: CatListContract.Presenter) {
            addToFavButton.setOnClickListener {
                presenter.saveToFavourites(item)
            }
            downloadImageButton.setOnClickListener {
                val context = itemView.context
                if (!PermissionUtil.hasPermission(context, WRITE_EXTERNAL_STORAGE_PERMISSION)) {
                    dealWithWriteToExternalStoragePermission(context, item)
                } else {
                    presenter.downloadImage(item)
                }
            }
            Glide.with(itemView)
                .load(item.url)
                .into(image)
        }

        private fun dealWithWriteToExternalStoragePermission(context: Context, item: Cat) {
            downloadUrl = item.url
            if (PermissionUtil.shouldShowRationale(
                    context as Activity,
                    WRITE_EXTERNAL_STORAGE_PERMISSION
                )
            ) {
                PermissionUtil.showRationaleAndRequestPermission(
                    context,
                    WRITE_EXTERNAL_STORAGE_PERMISSION,
                    REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE,
                    context.getString(R.string.write_to_external_rationale)
                )
            } else {
                PermissionUtil.requestPermission(
                    context,
                    WRITE_EXTERNAL_STORAGE_PERMISSION,
                    REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Cat>() {

            override fun areItemsTheSame(oldItem: Cat, newItem: Cat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Cat, newItem: Cat): Boolean {
                return oldItem == newItem
            }
        }
    }
}
