package com.develop.dubhad.catapp.ui.catlist

import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.ui.BaseContract

class CatListContract {

    interface View : BaseContract.View {
        fun loadDataSuccess(list: List<Cat>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadData()
        fun saveToFavourites(cat: Cat)
        fun downloadImage(cat: Cat)
    }
}
