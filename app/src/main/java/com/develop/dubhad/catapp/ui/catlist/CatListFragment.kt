package com.develop.dubhad.catapp.ui.catlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.develop.dubhad.catapp.R
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.di.component.DaggerFragmentComponent
import com.develop.dubhad.catapp.di.module.FragmentModule
import javax.inject.Inject

class CatListFragment : Fragment(), CatListContract.View {

    @Inject
    lateinit var presenter: CatListContract.Presenter

    private lateinit var catListView: RecyclerView
    private lateinit var catListAdapter: CatListAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val catListComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()
        catListComponent.inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_cat_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh_cat_list -> {
                presenter.loadData()
                true
            }
            else -> false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cat_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)
        swipeRefreshLayout.setOnRefreshListener { presenter.loadData() }
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )

        catListView = view.findViewById(R.id.cat_list)
        catListAdapter = CatListAdapter(presenter)
        catListView.adapter = catListAdapter

        presenter.attach(this)
        presenter.subscribe()

        presenter.loadData()
    }

    override fun onDestroyView() {
        presenter.unsubscribe()

        super.onDestroyView()
    }

    override fun loadDataSuccess(list: List<Cat>) {
        catListAdapter.submitList(list)
        swipeRefreshLayout.isRefreshing = false
    }
}
