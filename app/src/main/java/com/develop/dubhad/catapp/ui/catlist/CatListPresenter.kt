package com.develop.dubhad.catapp.ui.catlist

import com.develop.dubhad.catapp.App
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.cat.CatDao
import com.develop.dubhad.catapp.di.component.DaggerRoomComponent
import com.develop.dubhad.catapp.di.module.RoomModule
import com.develop.dubhad.catapp.network.CatApi
import com.develop.dubhad.catapp.utilities.DownloadUtil
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CatListPresenter : CatListContract.Presenter {

    @Inject
    lateinit var catDao: CatDao

    private val subscriptions = CompositeDisposable()
    private val api = CatApi.instance
    private lateinit var view: CatListContract.View

    init {
        DaggerRoomComponent.builder()
            .roomModule(RoomModule(App.instance))
            .build()
            .inject(this)
    }

    override fun subscribe() {}

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: CatListContract.View) {
        this.view = view
    }

    override fun loadData() {
        val subscribe = api.getCats()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list: List<Cat>? ->
                view.loadDataSuccess(list!!)
            }
        subscriptions.add(subscribe)
    }

    override fun saveToFavourites(cat: Cat) {
        Completable.fromAction { catDao.insertCat(cat) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    override fun downloadImage(cat: Cat) {
        DownloadUtil.downloadFile(cat.url)
    }
}
