package com.develop.dubhad.catapp.ui.favouritelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.develop.dubhad.catapp.R
import com.develop.dubhad.catapp.cat.Cat

class FavouriteListAdapter(private val presenter: FavouriteListContract.Presenter) :
    ListAdapter<Cat, FavouriteListAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_favourite,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cat = getItem(position)
        holder.apply {
            bind(cat, presenter)
            itemView.tag = cat.id
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image: ImageView = itemView.findViewById(R.id.item_cat_image)
        private val removeFromFavButton: ImageButton =
            itemView.findViewById(R.id.item_remove_from_fav_button)

        fun bind(item: Cat, presenter: FavouriteListContract.Presenter) {
            removeFromFavButton.setOnClickListener {
                presenter.removeFromFavourites(item)
            }
            Glide.with(itemView)
                .load(item.url)
                .into(image)
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Cat>() {

            override fun areItemsTheSame(oldItem: Cat, newItem: Cat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Cat, newItem: Cat): Boolean {
                return oldItem == newItem
            }
        }
    }
}
