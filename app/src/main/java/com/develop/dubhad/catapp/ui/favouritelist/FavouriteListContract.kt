package com.develop.dubhad.catapp.ui.favouritelist

import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.ui.BaseContract

class FavouriteListContract {

    interface View : BaseContract.View {
        fun loadDataSuccess(list: List<Cat>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadData()
        fun removeFromFavourites(cat: Cat)
    }
}
