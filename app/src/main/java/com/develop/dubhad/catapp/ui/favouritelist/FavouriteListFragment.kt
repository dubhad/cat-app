package com.develop.dubhad.catapp.ui.favouritelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.catapp.R
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.di.component.DaggerFragmentComponent
import com.develop.dubhad.catapp.di.module.FragmentModule
import javax.inject.Inject

class FavouriteListFragment : Fragment(), FavouriteListContract.View {

    @Inject
    lateinit var presenter: FavouriteListContract.Presenter

    private lateinit var favouriteListView: RecyclerView
    private lateinit var favouriteListAdapter: FavouriteListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val favouriteListComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()
        favouriteListComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cat_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favouriteListView = view.findViewById(R.id.cat_list)
        favouriteListAdapter = FavouriteListAdapter(presenter)
        favouriteListView.adapter = favouriteListAdapter

        presenter.attach(this)
        presenter.subscribe()

        presenter.loadData()
    }

    override fun onDestroyView() {
        presenter.unsubscribe()

        super.onDestroyView()
    }

    override fun loadDataSuccess(list: List<Cat>) {
        favouriteListAdapter.submitList(list)
    }
}
