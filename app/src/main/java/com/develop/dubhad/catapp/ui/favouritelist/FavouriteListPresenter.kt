package com.develop.dubhad.catapp.ui.favouritelist

import com.develop.dubhad.catapp.App
import com.develop.dubhad.catapp.cat.Cat
import com.develop.dubhad.catapp.cat.CatDao
import com.develop.dubhad.catapp.di.component.DaggerRoomComponent
import com.develop.dubhad.catapp.di.module.RoomModule
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FavouriteListPresenter : FavouriteListContract.Presenter {

    @Inject
    lateinit var catDao: CatDao

    private val subscriptions = CompositeDisposable()
    private lateinit var view: FavouriteListContract.View

    init {
        DaggerRoomComponent.builder()
            .roomModule(RoomModule(App.instance))
            .build()
            .inject(this)
    }

    override fun subscribe() {}

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: FavouriteListContract.View) {
        this.view = view
    }

    override fun loadData() {
        val subscribe = catDao.getAllCats()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list: List<Cat>? ->
                view.loadDataSuccess(list!!)
            }
        subscriptions.add(subscribe)
    }

    override fun removeFromFavourites(cat: Cat) {
        Completable.fromAction { catDao.deleteCat(cat) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }
}
