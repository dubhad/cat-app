package com.develop.dubhad.catapp.ui.main

import com.develop.dubhad.catapp.ui.BaseContract

class MainContract {

    interface View : BaseContract.View {
        fun showCatListFragment()
        fun showFavouriteListFragment()
    }

    interface Presenter : BaseContract.Presenter<MainContract.View>
}
