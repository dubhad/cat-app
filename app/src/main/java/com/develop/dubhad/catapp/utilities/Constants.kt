package com.develop.dubhad.catapp.utilities

import android.Manifest

const val BASE_URL = "https://api.thecatapi.com/v1/"
const val DATABASE_NAME = "app-db"

const val REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 61125
const val WRITE_EXTERNAL_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE
