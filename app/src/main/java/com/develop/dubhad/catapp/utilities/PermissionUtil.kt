package com.develop.dubhad.catapp.utilities

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.develop.dubhad.catapp.R

object PermissionUtil {

    fun hasPermission(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(activity: Activity, permission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
    }

    fun shouldShowRationale(activity: Activity, permission: String): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
    }

    fun showRationaleAndRequestPermission(
        activity: Activity,
        permission: String,
        requestCode: Int,
        message: String
    ) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity.getString(R.string.permission_needed_message))
            .setMessage(message)
            .setPositiveButton(android.R.string.yes) { _, _ ->
                requestPermission(activity, permission, requestCode)
            }
            .create()
            .show()
    }
}
